import React from "react";

const AddProduct = () => {
  return (
    <div class="card w-50">
      <div class="card-body">
        <img src="" className="card-img" alt="" />
        <h5 class="card-title">Card title</h5>
        <p class="card-text">
          With supporting text below as a natural lead-in to additional content.
        </p>
        <a href="#h" class="btn btn-primary">
          Button
        </a>
      </div>
    </div>
  );
};

export default AddProduct;
