import {
  RouterProvider,
  createBrowserRouter,
  createRoutesFromElements,
  Route,
} from "react-router-dom";
import "./App.css";
import RootLayout from "./layout/RootLayout";
import Home from "./components/Home";
import Products from "./components/Products";
import { useAxios } from "./hooks/useAxios";
import ProductLayout from "./layout/ProductLayout";
import ProductDetail from "./components/ProductDetail";
import { createContext, useState } from "react";
import Basket from "./components/Basket";

// export const BasketContext = createContext();

export const BasketContext = createContext();

function App() {
  const [basket, setBasket] = useState([]);
  console.log(basket);

  const addBasket = (param) => setBasket((prev) => [...prev, param]);

  const deleteBasket = (id) => setBasket(basket.filter((_) => _.id !== id));

  const routes = createBrowserRouter(
    createRoutesFromElements(
      <Route path="/" element={<RootLayout />}>
        <Route index element={<Home />} />
        <Route path={"products"} element={<ProductLayout />}>
          <Route index element={<Products />} />
          <Route path={":id"} element={<ProductDetail />} />
        </Route>
        <Route path="carts" element={<Basket />} />
      </Route>
    )
  );

  return (
    <BasketContext.Provider value={{ basket, addBasket, deleteBasket }}>
      <div className="App">
        <RouterProvider router={routes} />
      </div>
    </BasketContext.Provider>
  );
}

export default App;
