import React, { useContext } from "react";
import { BasketContext } from "../App";
// import Product from "./Product";

const Basket = () => {
  const { basket } = useContext(BasketContext);
  return (
    <>
      <div>
        {basket.map((item) => {
          return (
            <div className="border-1 p-5 mb-4 display-3" key={item.title}>
              {item.title}
            </div>
          );
        })}
      </div>
    </>
  );
};

export default Basket;
